﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace CXRefCards.Web.Models
{
    public class Card
    {
        [Key]
        public string Id { get; set; }
        public string Title { get; set; }
        public string Authors { get; set; }
        public string Image { get; set; }

        public string DescriptionInHtml { get; set; }
    }
}