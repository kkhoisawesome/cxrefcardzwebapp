﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CXRefCards.Web.Startup))]
namespace CXRefCards.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
